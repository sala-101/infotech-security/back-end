from flask_login import UserMixin
from flask_sqlalchemy import SQLAlchemy

db = SQLAlchemy()


# Tagi = rfid
class logs(db.Model):
    id_logs = db.Column(db.BIGINT, primary_key=True, unique=True, autoincrement=True)
    time_stamps = db.Column(db.VARCHAR(45))
    room_name = db.Column(db.VARCHAR(45))
    users_id_users = db.Column(db.BIGINT, primary_key=True, unique=True, nullable=True)
    error_code = db.Column(db.VARCHAR(70))

    def __init__(self, time_stamp, room_name, users_id_users, error_code):
        self.time_stamps = time_stamp
        self.room_name = room_name
        self.users_id_users = users_id_users
        self.error_code = error_code


class privileges(db.Model):
    id_privileges = db.Column(db.Integer, primary_key=True, unique=True, autoincrement=True)
    users_id_users = db.Column(db.VARCHAR(45), primary_key=True, unique=True)
    room_name = db.Column(db.VARCHAR(45))
    data_start = db.Column(db.VARCHAR(45))
    data_end = db.Column(db.VARCHAR(45))

    def __init__(self, user_id, numer_sali, data_start, data_end):
        self.users_id_users = user_id
        self.room_name = numer_sali
        self.data_start = data_start
        self.data_end = data_end


class week_days(db.Model):
    id_week_days = db.Column(db.BIGINT, primary_key=True, unique=True, autoincrement=True)
    monday = db.Column(db.Boolean)
    tuesday = db.Column(db.Boolean)
    wednesday = db.Column(db.Boolean)
    thurdsday = db.Column(db.Boolean)
    friday = db.Column(db.Boolean)
    saturday = db.Column(db.Boolean)
    sunday = db.Column(db.Boolean)
    privileges_id_privileges = db.Column(db.BIGINT, primary_key=True, unique=True)
    privileges_users_id_users = db.Column(db.BIGINT, primary_key=True, unique=True)

    def __init__(self, a, b, c, d, e, f, g, id_priv, user_id):
        self.monday = a
        self.tuesday = b
        self.wednesday = c
        self.thurdsday = d
        self.friday = e
        self.saturday = f
        self.sunday = g
        self.privileges_id_privileges = id_priv
        self.privileges_users_id_users = user_id


class users(db.Model, UserMixin):
    id_users = db.Column(db.BIGINT, primary_key=True, unique=True, autoincrement=True)
    oddzial = db.Column(db.VARCHAR(45))
    klasa = db.Column(db.Integer)
    imie = db.Column(db.VARCHAR(45))
    nazwisko = db.Column(db.VARCHAR(45))
    admin = db.Column(db.Boolean)
    block = db.Column(db.Boolean)
    tag = db.Column(db.VARCHAR(45))
    email = db.Column(db.VARCHAR(45))
    picture = db.Column(db.VARCHAR(100))

    def __init__(self, imie, nazwisko, email, admin, picture, oddzial, block, tag, klasa):
        self.imie = imie
        self.nazwisko = nazwisko
        self.email = email
        self.admin = admin
        self.picture = picture
        self.oddzial = oddzial
        self.block = block
        self.tag = tag
        self.klasa = klasa

    def get_id(self):
        return self.id_users
