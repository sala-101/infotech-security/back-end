from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from flask_cors import CORS


def config():
    app = Flask(__name__)

    app.config['SECRET_KEY'] = 'La34LfHbr@zJcPA33AL&'
    # Link do GCP
    app.config['SQLALCHEMY_DATABASE_URI'] = 'mysql://root:101_IoT@35.195.246.127:3306/is'
    app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = True
    app.config['JSON_AS_ASCII'] = False

    db = SQLAlchemy(app)
    CORS(app)
    return app, db


app, db = config()
