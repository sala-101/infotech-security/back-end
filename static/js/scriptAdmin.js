var button = window.document.getElementById("Scan");
button.addEventListener("click", scan);
function scan() {
    wd = window.open("", "_blank", "");
    wd.window.document.write("<h1>Przyłóż Tag !</h1>");
    fetch('https://admin.local:5000/scan', {mode: "cors", referrerPolicy: "unsafe-url"})
        .then(response => response.json())
        .then(data => {
            wd.window.document.getElementsByTagName("h1").value = "Tag: " + data["Tag"];
            document.getElementById("tag").value = `${data["Tag"]}`;
            wd.close();
        })
        .catch(error => {
            console.error(error);
            wd.close();
        })
}

function f() {
    fetch('https://admin.local:5000/check', {mode: "cors", referrerPolicy: "unsafe-url"})
        .then(response => response.json())
        .then(data => {
            if (data["message"] === 200){
                document.getElementById("Scan").style.display = "block";
            } else {
                document.getElementById("Scan").style.display = "none";
            }
        })
        .catch(error => {
            document.getElementById("Scan").style.display = "none";
        })
}
f();
setInterval(f, 5000);