from functools import wraps

import requests
import os
from flask import Flask
from flask_login import LoginManager, current_user
from oauthlib.oauth2 import WebApplicationClient
from models import users, db
import pymysql


def init_app():
    # Instalacja SQL na AWS
    pymysql.install_as_MySQLdb()

    '''
        Inicjalizacja Aplikacji
        - Nadanie nazwy
        - przypisanie folderów
        - nadanie hasła
    '''
    app = Flask("Infotech Security", static_folder='static', template_folder='templates')
    app.secret_key = "Js6C1q3C1VbJF91rxMfd"
    app.config['JSON_AS_ASCII'] = False

    # Nadanie linku dla bazy danych oraz wyłącznie błędu
    # Link do bazy na db4free
    # app.config['SQLALCHEMY_DATABASE_URI'] = 'mysql://is_tpi:101_3A_IoT@db4free.net:3306/is_tpi'
    app.config['SQLALCHEMY_DATABASE_URI'] = 'mysql://security:security@10.192.192.100:3306/security'
    app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = True
    app.config['SESSION_TYPE'] = 'filesystem'

    # Inicjalizacja SqlAlchemy (Narzędzia dla Bazy danych)
    db.init_app(app)

    # Nadanie dla zmiennych linków i haseł dla google oauth
    GOOGLE_CLIENT_ID = os.environ.get("GOOGLE_CLIENT_ID", "675116769295-c88nl1h33ceaq832bc2tepgjpb6m7a36.apps"
                                                          ".googleusercontent.com")
    GOOGLE_CLIENT_SECRET = os.environ.get("GOOGLE_CLIENT_SECRET", "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")
    GOOGLE_DISCOVERY_URL = "https://accounts.google.com/.well-known/openid-configuration"

    GOOGLE_APPLICATION_CREDENTIALS = "infotech-security-760fb1336251.json"

    # Inicjalizacja flask-login (Tworzenie instancji LoginManagera dzięki czemu można się logować)
    login_manager = LoginManager()
    login_manager.init_app(app)

    # Inicjalizacja OAuth klienta
    client = WebApplicationClient(GOOGLE_CLIENT_ID)

    def get_google_provider_cfg():
        return requests.get(GOOGLE_DISCOVERY_URL).json()

    @login_manager.user_loader
    def load_user(users_id):
        return users.query.get(users_id)

    return app, get_google_provider_cfg, client, GOOGLE_CLIENT_ID, GOOGLE_CLIENT_SECRET, GOOGLE_APPLICATION_CREDENTIALS


app, get_google_provider_cfg, client, GOOGLE_CLIENT_ID, GOOGLE_CLIENT_SECRET, GOOGLE_APPLICATION_CREDENTIALS = init_app()
