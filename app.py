import flask
import datetime as dt

import json
import logging

import pymysql
import requests
from flask import redirect, request, url_for, render_template, flash, jsonify
from flask_login import login_required, login_user, logout_user

from conf import app, get_google_provider_cfg, client, GOOGLE_CLIENT_ID, GOOGLE_CLIENT_SECRET, GOOGLE_APPLICATION_CREDENTIALS
from models import users, privileges, week_days, db, logs

from flask_pymongo import PyMongo

logging.basicConfig(level=logging.INFO)

pymysql.install_as_MySQLdb()

app.config['MONGO_URI'] = "mongodb://10.192.192.100:27017/ioT"
mongo = PyMongo(app)
mon_db = mongo.db


@app.route("/api/webhook", methods=['POST'])
def webhook():
    js = request.get_json()
    mon_db.ioT.insert_one({js})
    return jsonify({"status": 200}), 200


# Wyświetlanie wszystkich oraz wyświetlanie logów dla pojedynczej sali
@app.route('/logs', methods=['GET'])
def get_log():
    output = []
    for log in logs.query.all():
        log_data = {'id_log': str(log.id_logs), 'id_user': str(log.users_id_users), 'time': str(log.time_stamps),
                    'which scanner': str(log.room_name), 'type of error': str(log.error_code)}
        output.append(log_data)
    return flask.json.jsonify(({'logs': output}))


# Dodawanie logów
@app.route('/logs_check', methods=['GET'])
def post_logs():
    rfid = request.args.get("rfid")
    room = request.args.get("room")
    today = dt.datetime.today()
    if (rfid or room) is None:
        if room and rfid is None:
            logi = logs(today, "unknow", "unknow", "error")
        elif rfid is None:
            logi = logs(today, room, "unknow", "error")
        else:
            logi = logs(today, "unknow", rfid, "error")

        db.session.add(logi)
        db.session.commit()

        return 404
    isblocked, permission, room, ref_key, admin = check_user(rfid, room)
    error_code = get_return_code_name(isblocked, permission, admin)

    logi = logs(today, room, ref_key, error_code)

    db.session.add(logi)
    db.session.commit()

    return error_code


# Sprawdzanie czy można wejść
@app.route('/check', methods=['GET'])
def check_user(rfid, room):
    today = dt.datetime.today()

    ref_key, admin = get_ref_key(rfid)
    if admin != 1:
        isblocked = check_if_blocked(rfid, ref_key)
        permission = check_if_allowed(isblocked, ref_key, room, today)
    else:
        isblocked = check_if_blocked(rfid, ref_key)
        permission = 1
        return isblocked, permission, room, ref_key, admin
    return isblocked, permission, room, ref_key, admin


# Sprawdzanie czy tego dnia ma pozwolenie
def check_day(ref_key, ref_key_priv):
    nod = dt.datetime.now()
    a = nod.strftime("%A")
    try:
        if a == "Monday":
            if week_days.query.filter_by(privileges_users_id_users=ref_key,
                                         privileges_id_privileges=ref_key_priv).first().monday == 1:
                return 1
            else:
                return 0
        elif a == "Tuesday":
            if week_days.query.filter_by(privileges_users_id_users=ref_key,
                                         privileges_id_privileges=ref_key_priv).first().tuesday == 1:
                return 1
            else:
                return 0
        elif a == "Wednesday":
            if week_days.query.filter_by(privileges_users_id_users=ref_key,
                                         privileges_id_privileges=ref_key_priv).first().wednesday == 1:
                return 1
            else:
                return 0
        elif a == "Thursday":
            if week_days.query.filter_by(privileges_users_id_users=ref_key,
                                         privileges_id_privileges=ref_key_priv).first().thurdsday == 1:
                return 1
            else:
                return 0
        elif a == "Friday":
            if week_days.query.filter_by(privileges_users_id_users=ref_key,
                                         privileges_id_privileges=ref_key_priv).first().friday == 1:
                return 1
            else:
                return 0
        elif a == "Saturday":
            if week_days.query.filter_by(privileges_users_id_users=ref_key,
                                         privileges_id_privileges=ref_key_priv).first().saturday == 1:
                return 1
            else:
                return 0
        else:
            if week_days.query.filter_by(privileges_users_id_users=ref_key,
                                         privileges_id_privileges=ref_key_priv).first().sunday == 1:
                return 1
            else:
                return 0
    except AttributeError:
        return 0


# Wyszukiwanie ref_key
def get_ref_key(rfid):
    ref_key = users.query.filter_by(tag=rfid).first().id_users
    admin = users.query.filter_by(tag=rfid).first().admin
    if admin is None:
        admin = 0
    if ref_key is None:
        add_user = users(rfid, 1)
        db.session.add(add_user)
        db.session.commit()
        ref_key = add_user.id_users
    return ref_key, admin


# Sprawdzanie czy użytkownik jest zablokowany
def check_if_blocked(rfid, ref_key):
    isblocked = users.query.filter_by(tag=rfid).first().block
    if isblocked is None:
        isblocked = 1
    if ref_key is None:
        add_user = users(rfid, 1)
        db.session.add(add_user)
        db.session.commit()
        isblocked = add_user.block
    return isblocked


# Sprawdzanie czy użytkownik może wejść
def check_if_allowed(isblocked, ref_key, room, today):
    if isblocked != 1:
        try:
            data_time_start = privileges.query.filter_by(users_id_users=ref_key).first().data_start
            date_time_end = privileges.query.filter_by(users_id_users=ref_key).first().data_end
        except AttributeError:
            return 0
        if room == privileges.query.filter_by(room_name=room, users_id_users=ref_key).first().room_name:
            var1 = dt.datetime.strptime(date_time_end, '%Y-%m-%d')
            var2 = dt.datetime.strptime(data_time_start, '%Y-%m-%d')
            if var1 >= today >= var2:
                ref_key_priv = privileges.query.filter_by(room_name=room, users_id_users=ref_key).first().id_privileges
                if check_day(ref_key, ref_key_priv) == 1:
                    return 1
    return 0


# Otrzymywanie nazwy kodu
def get_return_code_name(isblocked, permission, admin):
    if admin == 1:
        return "200: User is admin"
    if isblocked == 0:
        if permission == 0:
            return "403: User is not allowed to enter"
    else:
        return "403: User is blocked"
    return "200: User is allowed to open the doors"


@app.errorhandler(404)
def not_found(e):
    return render_template("404.html")


@app.errorhandler(401)
def acess_denied(e):
    return render_template("401.html")


@app.errorhandler(500)
def unexpected(e):
    return render_template("500.html")


@app.route('/')
def index():
    return render_template('index.html')


@app.route('/login')
def login():
    return render_template('signup.html')


@app.route('/login/auth')
def login_auth():
    google_provider_cfg = get_google_provider_cfg()
    authorization_endpoint = google_provider_cfg["authorization_endpoint"]

    uri = client.prepare_request_uri(
        authorization_endpoint,
        # Do testów na local
        # redirect_uri=request.base_url + '/callback',
        # Do wygrywania na aws
        redirect_uri='https://ir.infotech.edu.pl:5000/login/auth/callback',
        # redirect_uri='https://2gj805yybg.execute-api.eu-central-1.amazonaws.com/dev/login/auth/callback',
        scope=["openid", "email", "profile"])
    return redirect(uri)


@app.route('/login/auth/callback')
def login_auth_callback():
    code = request.args.get('code')
    domain = request.args.get('hd')
    if domain != 'infotech.edu.pl':
        flash('Musisz mieć e-mail w infotech.edu.pl')
        return redirect(url_for('login'))

    google_provider_cfg = get_google_provider_cfg()
    token_endpoint = google_provider_cfg["token_endpoint"]

    token_url, headers, body = client.prepare_token_request(
        token_endpoint,
        authorization_response=request.url,
        redirect_url=request.base_url,
        code=code
    )
    token_response = requests.post(
        token_url,
        headers=headers,
        data=body,
        auth=(GOOGLE_CLIENT_ID, GOOGLE_CLIENT_SECRET),
    )

    client.parse_request_body_response(json.dumps(token_response.json()))

    userinfo_endpoint = google_provider_cfg["userinfo_endpoint"]
    uri, headers, body = client.add_token(userinfo_endpoint)
    userinfo_response = requests.get(uri, headers=headers, data=body)
    if userinfo_response.json().get("email_verified"):
        # unique_id = userinfo_response.json()["sub"] Id od google
        users_email = userinfo_response.json()["email"]
        picture = userinfo_response.json()["picture"]
        users_name = userinfo_response.json()["given_name"]
        family_name = userinfo_response.json()["family_name"]
    else:
        return "User email not available or not verified by Google.", 400

    exites_user = users.query.filter_by(email=users_email).first()

    if not exites_user:
        flash('Konto nie istnieje')
        return redirect(url_for('index'))

    if not exites_user.admin:
        flash("Brak dostępu z tego konta")
        return redirect(url_for("index"))

    if exites_user.block:
        flash('To konto jest zablokowane skontakuj się z Administratorem systemu')
        return redirect(url_for("index"))

    if exites_user.imie != users_name or exites_user.nazwisko != family_name or exites_user.picture != picture:
        exites_user.imie = users_name
        exites_user.nazwisko = family_name
        exites_user.picture = picture
        db.session.commit()

    login_user(exites_user)

    flash("Zalogowano")
    return redirect(url_for("index"))


@app.route('/logout')
@login_required
def logout():
    logout_user()
    return redirect(url_for('index'))


@app.route('/controlpanel')
@login_required
def controlpanel():
    return render_template('controlpanel.html',)


@app.route('/infrastruktura')
@login_required
def infrastruktura():
    user_rows = users.query.all()
    return render_template('locks.html', user_rows=user_rows)


@app.route('/locks_edit')
@login_required
def locks_edit():
    imie = request.args.get('imie')
    nazwisko = request.args.get('nazwisko')
    user_id = request.args.get('user_id')
    return render_template('locks-edit.html', imie=imie, nazwisko=nazwisko, users_id_users=user_id)


def handle_checkbox(checkbox):
    if checkbox is not None:
        return 1
    else:
        return 0


@app.route('/handle_locks_edit', methods=['POST'])
@login_required
def handle_locks_edit():
    user_id = request.args.get('id')
    numer_sal = request.form.get('numer_sali')
    data_start = request.form.get('data_start')
    data_end = request.form.get('data_end')

    pon = handle_checkbox(request.form.get("monday"))
    wto = handle_checkbox(request.form.get("tuseday"))
    sro = handle_checkbox(request.form.get("wednesday"))
    czw = handle_checkbox(request.form.get("thr"))
    pia = handle_checkbox(request.form.get("friday"))
    sob = handle_checkbox(request.form.get("saturday"))
    nied = handle_checkbox(request.form.get("sunday"))

    new_permission = privileges(user_id, numer_sal, data_start, data_end)
    db.session.add(new_permission)
    db.session.commit()

    new_week_days = week_days(pon, wto, sro, czw, pia, sob, nied, new_permission.id_privileges, user_id)
    db.session.add(new_week_days)
    db.session.commit()
    return redirect(url_for('infrastruktura'))


@app.route('/locks_fill_data')
@login_required
def locks_fill_data():
    user_id = request.args.get('user_id')
    bool = request.args.get('bool')
    user_rows = users.query.all()
    return render_template("locks-fill-data.html", users_id_users=user_id, user_rows=user_rows, force_fill=bool)


@app.route('/handle_locks_data', methods=['POST'])
@login_required
def handle_locks_data():
    logger = logging.getLogger()

    user_id = request.args.get('id')
    oddzial = request.form.get('oddzial')
    imie = request.form.get('imie')
    nazwisko = request.form.get('nazwisko')
    klasa = request.form.get('klasa')  # int
    tag = request.form.get('tag')
    email = request.form.get('email')

    user_from_logs = users.query.filter_by(tag=tag).first()
    nulls = 0

    if not user_from_logs:
        pass
    else:
        if user_from_logs.oddzial is None:
            nulls += 1
        if user_from_logs.klasa is None:
            nulls+=1
        if user_from_logs.imie is None:
            nulls+=1
        if user_from_logs.nazwisko is None:
            nulls+=1
        if user_from_logs.email is None:
            nulls+=1
        if user_from_logs.picture is None:
            nulls+=1

    if nulls >= 5:
        db.session.query(logs).filter(logs.users_id_users == user_from_logs.id_users).update({logs.users_id_users: user_id}, synchronize_session=False)
        db.session.delete(user_from_logs)

    try:
        user_current = users.query.filter_by(id_users=user_id).first()
        user_current.tag = str(tag)
        user_current.oddzial = str(oddzial)
        user_current.imie = str(imie)
        user_current.nazwisko = str(nazwisko)
        user_current.klasa = int(klasa)
        user_current.email = str(email)
        db.session.commit()
    except Exception as x:
        logger.info(x)
        logger.error(x)

    return redirect(url_for('infrastruktura'))


@app.route('/privlilages', methods=['GET', 'POST'])
@login_required
def prv():
    imie = request.args.get('imie')
    nazwisko = request.args.get('nazwisko')
    user_id = request.args.get('user_id')
    uprawnienia = privileges.query.all()
    dni_tygodnia = week_days.query.all()
    return render_template('user_privilages.html', imie=imie, nazwisko=nazwisko, users_id_users=user_id,
                           prv=uprawnienia, dni=dni_tygodnia)


@app.route('/admin_or_block', methods=['POST', "GET"])
@login_required
def admin_or_block_handler():
    user_id = request.args.get('id')
    is_admin = request.form.get('admin')
    is_blocked = request.form.get('block')

    if is_admin is None:
        is_admin = 0
    else:
        is_admin = 1

    if is_blocked is None:
        is_blocked = 0
    else:
        is_blocked = 1

    user_current = users.query.filter_by(id_users=user_id).first()
    user_current.block = is_blocked
    user_current.admin = is_admin
    db.session.commit()

    return redirect(url_for('infrastruktura'))


@app.route('/log')
@login_required
def logi():
    # logs_rows = logs.query.all()
    logs_rows = db.session.execute("SELECT l.time_stamps, "
                                   "l.room_name, "
                                   "u.imie, "
                                   "u.nazwisko, "
                                   "u.klasa, "
                                   "u.oddzial, "
                                   "u.tag, "
                                   "u.email, "
                                   "l.error_code from logs l "
                                   "left join users u on l.users_id_users = u.id_users "
                                   "ORDER BY l.id_logs DESC ")
    return render_template('logs.html', logs_rows=logs_rows)


@app.route('/profile')
def profile():
    return render_template('profile.html')


@app.route('/settings')
def settings():
    return render_template('settings.html')


@app.route('/wiki')
def wiki():
    return render_template('wikipedia.html')


@app.route('/support')
def support():
    return render_template('support.html')


if __name__ == '__main__':
    app.run(host='0.0.0.0', ssl_context='adhoc', debug='on', port=5000)
